package com.inter.InterApi.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

public class CadastroUsuarioDto {
	
	private String nome;
	private String email;
	
	public CadastroUsuarioDto() {}
	
	@NotEmpty(message = "Nome não pode ser vazio.")
	@Length(min = 10, max = 200, message = "Nome deve conter entre 10 e 200 caracteres.")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@NotEmpty(message = "Email não pode ser vazio.")
	@Length(min = 8, max = 200, message = "Email deve conter entre 8 e 200 caracteres.")
	@Email(message="Email inválido.")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "CadastroUsuarioDto [nome=" + nome + ", email=" + email + "]";
	}

}
