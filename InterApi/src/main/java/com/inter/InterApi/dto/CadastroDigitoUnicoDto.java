package com.inter.InterApi.dto;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

public class CadastroDigitoUnicoDto {
	
	private int digito;
	private int repeticao;

	public CadastroDigitoUnicoDto() {}

	@NotEmpty(message = "Digito não pode ser vazio.")
	@Length(min = 1, max = 10000000, message = "Dígito deve conter entre 1 e 1^10000000 dígitos.")
	public int getDigito() {
		return digito;
	}

	public void setDigito(int digito) {
		this.digito = digito;
	}

	@NotEmpty(message = "Número de repetições não pode ser vazio.")
	@Length(min = 1, max = 100000, message = "Dígito deve conter entre 1 e 10^5 dígitos.")
	public int getRepeticao() {
		return repeticao;
	}

	public void setRepeticao(int repeticao) {
		this.repeticao = repeticao;
	}

	@Override
	public String toString() {
		return "DigitoUnicoDto [digito=" + digito + ", repeticao=" + repeticao + ", getDigito()=" + getDigito()
				+ ", getRepeticao()=" + getRepeticao() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
