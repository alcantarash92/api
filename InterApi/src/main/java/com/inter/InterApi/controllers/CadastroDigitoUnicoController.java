package com.inter.InterApi.controllers;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inter.InterApi.entities.DigitoUnico;
import com.inter.InterApi.entities.Usuario;
import com.inter.InterApi.response.Response;
import com.inter.InterApi.services.DigitoUnicoService;
import com.inter.InterApi.services.UsuarioService;
import com.inter.InterApi.dto.CadastroDigitoUnicoDto;
import com.inter.InterApi.dto.DigitoUnicoDto;

@RestController
@RequestMapping("/api/digito-unico")
@CrossOrigin(origins = "*")
public class CadastroDigitoUnicoController {
	
	private static final Logger log = LoggerFactory.getLogger(CadastroDigitoUnicoController.class);
	
	@Autowired
	private DigitoUnicoService digitoUnicoService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	public CadastroDigitoUnicoController() {}
	
	/**
	 * Cadastra um digito único.
	 * 
	 * @param cadastroDigitoUnicoDto
	 * @param result
	 * @return ResponseEntity<Response<CadastroDigitoUnicoDto>>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping
	public ResponseEntity<Response<CadastroDigitoUnicoDto>> cadastrar(@Valid @RequestBody CadastroDigitoUnicoDto cadastroDigitoUnicoDto,
			BindingResult result) throws NoSuchAlgorithmException {
		log.info("Cadastrando Digito: {}", cadastroDigitoUnicoDto.toString());
		Response<CadastroDigitoUnicoDto> response = new Response<CadastroDigitoUnicoDto>();

		validarDadosExistentes(cadastroDigitoUnicoDto, result);
		DigitoUnico digitoUnico = this.converterDtoParaDigitoUnico(cadastroDigitoUnicoDto, result);

		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro PF: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		Optional<DigitoUnicoRepository> digitoUnico = this.digitoUnicoService.buscarPorDigito(cadastroDigitoUnicoDto.getDigito());
		digitoUnico.ifPresent(dig -> digitoUnico.);
		this.digitoUnicoService.persistir(digitoUnico);

		response.setData(this.converterCadastroPFDto(funcionario));
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Verifica se o digito está cadastrado e se o usuário não existe na base de dados.
	 * 
	 * @param cadastroDigitoUnicoDto
	 * @param result
	 */
	private void validarDadosExistentes(CadastroDigitoUnicoDto cadastroDigitoUnicoDto, BindingResult result) {
		Optional<Usuario> usuario = Optional.empty();
		if (!usuario.isPresent()) {
			result.addError(new ObjectError("usuario", "Usuário não cadastrado."));
		}
		
		this.digitoUnicoService.buscarPorDigito(cadastroDigitoUnicoDto.getDigito())
			.ifPresent(func -> result.addError(new ObjectError("digito", "Digito já existente.")));
	}

}
