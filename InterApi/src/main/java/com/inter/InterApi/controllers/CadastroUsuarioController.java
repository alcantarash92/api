package com.inter.InterApi.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inter.InterApi.dto.CadastroDigitoUnicoDto;
import com.inter.InterApi.dto.CadastroUsuarioDto;
import com.inter.InterApi.entities.DigitoUnico;
import com.inter.InterApi.entities.Usuario;
import com.inter.InterApi.response.Response;
import com.inter.InterApi.services.DigitoUnicoService;
import com.inter.InterApi.services.UsuarioService;

@RestController
@RequestMapping("/api/cadastrar-usuario")
@CrossOrigin(origins = "*")//Alterar depois para deixar mais seguro
public class CadastroUsuarioController {
	
	private static final Logger log = LoggerFactory.getLogger(CadastroUsuarioController.class);
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private DigitoUnicoService digitoUnicoService;
	
	public CadastroUsuarioController() {}
	
	/**
	 * Cadastra um usuario.
	 * 
	 * @param CadastroUsuarioDto
	 * @param result
	 * @return ResponseEntity<Response<CadastroUsuarioDto>>
	 * @throws NoSuchAlgorithmException
	 */
	
	public ResponseEntity<Response<CadastroUsuarioDto>> cadastrar(@Valid @RequestBody CadastroUsuarioDto cadastroUsuarioDto,
			BindingResult result) throws NoSuchAlgorithmException {
		
		log.info("Cadastrando Usuario: {}", cadastroUsuarioDto.toString());
		Response<CadastroUsuarioDto> response = new Response<CadastroUsuarioDto>();
		
		validarDadosExistentes(cadastroUsuarioDto, result);
		Usuario usuario = this.converterDtoParaUsuario(cadastroUsuarioDto, result);
		//DigitoUnico digitoUnico = this.converterDtoParaDigitoUnico(cadastroUsuarioDto, result);
		
		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro de Usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
			
			this.usuarioService.persistir(usuario);
			

			//response.setData(this.converterDtoParaDigitoUnico(usuario));
			return ResponseEntity.ok(response);
		}
	}
	
	/**
	 * Por enquanto verifica se o email está cadastrado e se o email não existe na base de dados.
	 * 
	 * @param CadastroUsuarioDto
	 * @param result
	 */
	private void validarDadosExistentes(CadastroUsuarioDto cadastroUsuarioDto, BindingResult result) {
		Optional<Usuario> email = this.usuarioService.buscarPorEmail(cadastroUsuarioDto.getEmail());

		this.usuarioService.buscarPorEmail(cadastroUsuarioDto.getEmail())
			.ifPresent(usu -> result.addError(new ObjectError("email", "Email já existente.")));
	}
	
	/**
	 * Converte os dados do DTO para usuário.
	 * 
	 * @param cadastroUsuarioDto
	 * @param result
	 * @return Usuario
	 * @throws NoSuchAlgorithmException
	 */
	private Usuario converterDtoParaUsuario(CadastroUsuarioDto cadastroUsuarioDto, BindingResult result)
			throws NoSuchAlgorithmException {
		Usuario usuario = new Usuario();
		usuario.setNome(cadastroUsuarioDto.getNome());
		usuario.setEmail(cadastroUsuarioDto.getEmail());

		return usuario;
	}
	
	/*
	private DigitoUnico converterDtoParaDigitoUnico(CadastroDigitoUnicoDto cadastroDigitoUnico, BindingResult result)
			throws NoSuchAlgorithmException {
		
	}*/
	
	/**
	 * Converte os dados do DTO para DigitoUnico.
	 * 
	 * @param cadastroUsuarioDto
	 * @param result
	 * @return DigitoUnico
	 * @throws NoSuchAlgorithmException
	 */
	private CadastroUsuarioDto converterDCadastroUsuarioDto(Usuario usuario) {
		CadastroUsuarioDto cadastroUsuarioDto = new CadastroUsuarioDto();
		cadastroUsuarioDto.setNome(cadastroUsuarioDto.getNome());
		cadastroUsuarioDto.setEmail(cadastroUsuarioDto.getEmail());


		return cadastroUsuarioDto;
	}	

}
