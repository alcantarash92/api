package com.inter.InterApi.controllers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.inter.InterApi.dto.DigitoUnicoDto;
import com.inter.InterApi.entities.DigitoUnico;
import com.inter.InterApi.response.Response;
import com.inter.InterApi.services.DigitoUnicoService;

public class DigitoUnicoController {
	
	private static final Logger log = LoggerFactory.getLogger(DigitoUnicoController.class);
	
	@Autowired
	private DigitoUnicoService digitoUnicoService;
	
	public DigitoUnicoController() {}
	
	/**
	 * Retorna um digitoUnico dado um digito.
	 * 
	 * @param digito
	 * @return ResponseEntity<Response<DigitoUnicoDto>>
	 */
	@GetMapping(value = "/digitoUnico/{digitoUnico}")
	public ResponseEntity<Response<DigitoUnicoDto>> buscarPorDigito(@PathVariable("digitoU") int digitoU) {
		log.info("Buscando Digito Unico: {} ", digitoU);
		Response<DigitoUnicoDto> response = new Response<DigitoUnicoDto>();
		Optional<DigitoUnico>  digito = digitoUnicoService.buscarPorDigito(digitoU);

		response.setData(this.converterDigitoUnicoDto(digito.get()));
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Popula um DTO com os dados de uma empresa.
	 * 
	 * @param empresa
	 * @return EmpresaDto
	 */
	private DigitoUnicoDto converterDigitoUnicoDto(DigitoUnico digitoUnico) {
		DigitoUnicoDto digitoUnicoDto = new DigitoUnicoDto();
		digitoUnicoDto.setDigito(digitoUnico.getDigito());
		digitoUnicoDto.setRepeticao(digitoUnico.getRepeticao());

		return digitoUnicoDto;
	}

}
