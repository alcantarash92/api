package com.inter.InterApi.controllers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inter.InterApi.dto.UsuarioDto;
import com.inter.InterApi.entities.Usuario;
import com.inter.InterApi.response.Response;
import com.inter.InterApi.services.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
@CrossOrigin(origins = "*")
public class UsuarioController {
	
	private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);
	
	@Autowired
	private UsuarioService usuarioService;
	
	public UsuarioController() {}
	
	/**
	 * Retorna um usuario dado um EMAIL.
	 * 
	 * @param email
	 * @return ResponseEntity<Response<Usuariodto>>
	 */
	@GetMapping(value = "/email/{email}")
	public ResponseEntity<Response<UsuarioDto>> buscarPorEmail(@PathVariable("email") String email) {
		log.info("Buscando usuario por email: {}", email);
		Response<UsuarioDto> response = new Response<UsuarioDto>();
		Optional<Usuario> usuario = usuarioService.buscarPorEmail(email);

		if (!usuario.isPresent()) {
			log.info("Usuario não encontradao para o EMAIL: {}", email);
			response.getErrors().add("Esuario não encontrado para o EMAIL " + email);
			return ResponseEntity.badRequest().body(response);
		}

		response.setData(this.converterUsuarioDto(usuario.get()));
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Popula um DTO com os dados de um usuário.
	 * 
	 * @param usuario
	 * @return UsuarioDto
	 */
	private UsuarioDto converterUsuarioDto(Usuario usuario) {
		UsuarioDto usuarioDto = new UsuarioDto();
		usuarioDto.setNome(usuario.getNome());
		usuarioDto.setEmail(usuarioDto.getEmail());

		return usuarioDto;
	}

}
