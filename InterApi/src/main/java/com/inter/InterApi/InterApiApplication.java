package com.inter.InterApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class InterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterApiApplication.class, args);
	}

}
