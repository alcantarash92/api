package com.inter.InterApi.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "digitoUnico")
public class DigitoUnico implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int digito;
	private int repeticao;
	
	private DigitoUnico() {}

	@Column(name = "digito", nullable = false)
	public int getDigito() {
		return digito;
	}

	public void setDigito(int digito) {
		this.digito = digito;
	}

	@Column(name = "repeticao", nullable = false)
	public int getRepeticao() {
		return repeticao;
	}

	public void setRepeticao(int repeticao) {
		this.repeticao = repeticao;
	}

	@Override
	public String toString() {
		return "DigitoUnico [digito=" + digito + ", repeticao=" + repeticao + "]";
	}	
}
