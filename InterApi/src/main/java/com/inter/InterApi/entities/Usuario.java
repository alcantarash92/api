package com.inter.InterApi.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String email;
	private List<DigitoUnico> digitoUnico;
	
	public Usuario() {}
	
	@Column(name = "email", nullable = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "nome", nullable = false)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@OneToMany(mappedBy = "digitoUnico", fetch = FetchType.EAGER)
	public List<DigitoUnico> getDigitoUnico() {
		return digitoUnico;
	}

	public void setDigitoUnico(List<DigitoUnico> digitoUnico) {
		this.digitoUnico = digitoUnico;
	}
	
	@Override
	public String toString() {
		return "Usuario [nome=" + nome + ", email=" + email + ", digitoUnico=" + digitoUnico + "]";
	}
}
