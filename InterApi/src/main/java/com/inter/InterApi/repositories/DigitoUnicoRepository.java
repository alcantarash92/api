package com.inter.InterApi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.inter.InterApi.entities.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {
	
	@Transactional(readOnly = true)
	DigitoUnico findByEmail(String email);

}
