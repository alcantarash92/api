package com.inter.InterApi.services;

import java.util.Optional;

import com.inter.InterApi.entities.DigitoUnico;

public interface DigitoUnicoService {
	
	/**
	 * Retorna um digito dado um Digito
	 * 
	 * @param digito
	 * @return Optional<DigitoUnico>
	 */
	Optional<DigitoUnico> buscarPorDigito(int digito);
	
	/**
	 * Cadastra um digito na base de dados.
	 * 
	 * @param digito
	 * @return digitoUnico
	 */
	DigitoUnico persistir(DigitoUnico digitoUnico);

}
