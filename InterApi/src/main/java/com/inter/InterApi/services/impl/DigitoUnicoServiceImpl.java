package com.inter.InterApi.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.inter.InterApi.entities.DigitoUnico;
import com.inter.InterApi.repositories.DigitoUnicoRepository;
import com.inter.InterApi.services.DigitoUnicoService;

public class DigitoUnicoServiceImpl implements DigitoUnicoService{
	
	private static final Logger log = LoggerFactory.getLogger(DigitoUnicoServiceImpl.class);
	
	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	@Override
	public Optional<DigitoUnico> buscarPorDigito(int digito) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DigitoUnico persistir(DigitoUnico digitoUnico) {
		// TODO Auto-generated method stub
		return null;
	}

}
