package com.inter.InterApi.services;

import java.util.Optional;

import com.inter.InterApi.entities.Usuario;

public interface UsuarioService {
	
	
	/**
	 * Retorna um usuario dado um EMAIL.-> depois mudar para CPF
	 * 
	 * @param email
	 * @return Optional<Usuario>
	 */
	Optional<Usuario> buscarPorEmail(String email);
	
	/**
	 * Cadastra um usuario na base de dados.
	 * 
	 * @param usuario
	 * @return usuario
	 */
	
	Usuario persistir(Usuario usuario);

}
